//FirstView Component Constructor
function FirstView() {
	//create object instance, a parasitic subclass of Observable
	var self = Ti.UI.createView();
	
	//label using localization-ready strings from <app dir>/i18n/en/strings.xml
	var label = Ti.UI.createLabel({
		color:'#000000',
		text:'Mobile Accessible Tweets'+'\n'+'본 어플리케이션은\n시각장애인 사용자를 위한\n트위터 어플리케이션으로\n2012년 서울대학교 QoLT Tadis 공모전에\n출품되었습니다.\n\n화면을 두 번 클릭하시면\n트위터 계정설정 페이지로 이동합니다.',
		height:'auto',
		width:'auto',
		textAlign:'center'
	});
	self.add(label);
	
	self.addEventListener('click', function(e) {
		movePage('click', self);
	});
	
	return self;
}

function movePage(evt, self)
{
	Ti.include("/lib/twitter_api.js");
	//initialization
	twitterApi = new TwitterApi({
	    consumerKey:'JlWM1fqlDE5uXPaRcKUw',
	    consumerSecret:'BLrSvUrDrf9nETjtZVNVnJUYbKen4uxG5ifZR7sta4'
	});
	twitterApi.init();
	
	if(twitterApi != null)
	{
		var TimelineView = require('ui/TimelineView');
		var timelineview = new TimelineView();
		cur_win.remove(self);
		cur_win.add(timelineview);
	}
}
module.exports = FirstView;
