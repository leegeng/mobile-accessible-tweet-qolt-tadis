//FirstView Component Constructor
function TimelineView() {
	//create object instance, a parasitic subclass of Observable
	var self = Ti.UI.createView();
	currentPage = 0;	
	//load tweets to memory
	twitterApi.statuses_home_timeline({
	    onSuccess: function(tweets){
	        /*for(var i=0;i<tweets.length;i++){
	            var tweet = tweets[i];
	            // now you can use tweet.user.name, tweet.text, etc..
	        }*/
	        twitData = tweets;
			main(self);
	    },
	    onError: function(error){
	        Ti.API.error(error);
	    }
	});
	return self;
}
module.exports = TimelineView;

function main(self)
{
   	var tweet = twitData[0];
	var text = Ti.UI.createLabel({
		text:tweet.user.name + "의 트윗\n"+tweet.text,
		width:300,
		height:240,
		top:0
	});	
	self.add(text);
	
	var btn_left = Ti.UI.createLabel({
		text:'이전',
		top:250,
		left:0,
		width:50,
		height:230,
		backgroundColor:'green'
	});	
	btn_left.addEventListener('click',function(e){
		if(currentPage > 0)
		{
			currentPage--;
			showText(text, currentPage);
		}
		else
			alert("이전 트윗이 없습니다.");
	});
	self.add(btn_left);
	
	var btn_right = Ti.UI.createLabel({
		text:'다음',
		top:250,
		right:0,
		width:50,
		height:230,
		backgroundColor:'green'
	});	
	btn_right.addEventListener('click',function(e){
		if(currentPage < twitData.length-1)
		{
			currentPage++;
			showText(text, currentPage);
		}
		else
			alert("다음 트윗이 없습니다.");
	});
	self.add(btn_right);

	var btn_tweet = Ti.UI.createLabel({
		text:'트윗 작성',
		top:250,
		left: 55,
		width:65,
		height:110,
		backgroundColor:'green'
	});
	btn_tweet.addEventListener('click', function(e){
		Tweet("tweet","");
	});
	self.add(btn_tweet);
	
	var btn_reply = Ti.UI.createLabel({
		text:'멘션 작성',
		top:250,
		left: 125,
		width:65,
		height:110,
		backgroundColor:'green'
	});
	btn_reply.addEventListener('click', function(e){
		var user = twitData[currentPage];
		Tweet("reply",user.user.screen_name);
	});
	self.add(btn_reply);
	
	var btn_retweet = Ti.UI.createLabel({
		text:'리트윗',
		top:250,
		left: 195,
		width:65,
		height:110,
		backgroundColor:'green'
	});
	btn_retweet.addEventListener('click', function(e){
		var tweet = twitData[currentPage];
		var msg = "\"@"+tweet.user.screen_name + ": "+tweet.text+" \"";
		Tweet('retweet',msg);
	});
	self.add(btn_retweet);
	
	var btn_mode = Ti.UI.createLabel({
		text:'멘션보기',
		top:370,
		left:55,
		width:65,
		height:110,
		backgroundColor:'green'
	});
	btn_mode.addEventListener('click', function(e){
		if(btn_mode.text == "멘션보기")
		{
			btn_mode.text = "타임라인보기";
			currentPage = 0;	
			//load tweets to memorystatuses_home_timeline
			twitterApi.statuses_mentions({
			    onSuccess: function(tweets){
			        /*for(var i=0;i<tweets.length;i++){
			            var tweet = tweets[i];
			            // now you can use tweet.user.name, tweet.text, etc..
			        }*/
			        twitData = tweets;
					showText(text, currentPage);
			    },
			    onError: function(error){
			        Ti.API.error(error);
			    }
			});
		}
		else
		{
			btn_mode.text = "멘션보기";
			currentPage = 0;	
			//load tweets to memory
			twitterApi.statuses_home_timeline({
			    onSuccess: function(tweets){
			        /*for(var i=0;i<tweets.length;i++){
			            var tweet = tweets[i];
			            // now you can use tweet.user.name, tweet.text, etc..
			        }*/
			        twitData = tweets;
					showText(text, currentPage);
			    },
			    onError: function(error){
			        Ti.API.error(error);
			    }
			});
		}
	});
	self.add(btn_mode);
	
	var btn_link = Ti.UI.createLabel({
		text:'링크이동',
		top:370,
		left:125,
		width:65,
		height:110,
		backgroundColor:'green'
	});
	btn_link.addEventListener('click', function(e){
		var tweet = twitData[currentPage];
		if(tweet.entities.urls[0] == null)
			alert("이동 가능한 링크가 없습니다.");
		else
		{
			var link = tweet.entities.urls[0].url;
			Titanium.Platform.openURL(link);
		}
	});
	self.add(btn_link);
	
	var btn_dm = Ti.UI.createLabel({
		text:'쪽지 보내기',
		top:370,
		left:195,
		width:65,
		height:110,
		backgroundColor:'green'
	});
	btn_dm.addEventListener('click', function(e){
		var user = twitData[currentPage];
		Tweet("dm",user.user.screen_name);
	});
	self.add(btn_dm);
}

function showText(text, index)
{
	var tweet = twitData[index];
	text.text = tweet.user.name + "의 트윗\n"+tweet.text;
}

function Tweet(type, text)
{
	var ios_tweet = require("de.marcelpociot.twitter");
	var msg = "";
	if(type == "tweet")
	{
		msg = "";
	}
	else if(type=="reply")
	{
		msg = "@"+text+" ";
	}
	else if(type == "retweet")
	{
		msg = text;
	}
	else if(type == "dm")
	{
		msg = "DM @"+text+" ";		
	}
	ios_tweet.tweet({
		message: 	msg,
		succes:		function(){
			alert("Tweet successfully sent");
		},
		cancel:		function(){
			alert("User canceled tweet");
		},
		error:		function(){
			alert("Unable to send tweet");
		}
	});
}